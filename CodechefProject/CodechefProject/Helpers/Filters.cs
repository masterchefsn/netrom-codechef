﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodechefProject.Constants;
using CodechefProject.Models.Menu;
using CodechefProject.Models.User;

namespace CodechefProject.Helpers
{
    public static class Filters
    {
        public static bool NoMeat(string desc)
        {
            return !Util.IsInList(KeyWords.PuiList, desc) && !Util.IsInList(KeyWords.PorcList, desc)
                   && !Util.IsInList(KeyWords.VitaList, desc);
        }

        public static bool NoMeatOrFish(string desc)
        {
            return NoMeat(desc) && NoFish(desc);
        }

        public static bool NoFish(string desc)
        {
            return !Util.IsInList(KeyWords.PesteList, desc);
        }

        public static bool ContainsMeat(string menuDescription)
        {
            return Util.IsInList(KeyWords.PuiList, menuDescription)
                   || Util.IsInList(KeyWords.PorcList, menuDescription)
                   || Util.IsInList(KeyWords.PesteList, menuDescription)
                   || Util.IsInList(KeyWords.VitaList, menuDescription);
        }

        public static bool IsCiorba(string menuDescription)
        {
            return Util.IsInList(KeyWords.CiorbeList, menuDescription);
        }

        public static bool IsPostMeal(string description)
        {
            return Util.IsInList(KeyWords.postList, description);
        }

        public static bool IsFruct(string description)
        {
            return Util.IsInList(KeyWords.FructeList, description);
        }

        public static bool IsPrajitura(string mealDescription)
        {
            return Util.IsInList(KeyWords.PrajituriList, mealDescription);
        }

        public static bool IsCrema(string mealDescription)
        {
            return Util.IsInList(KeyWords.CremeList, mealDescription);
        }

        public static bool IsSalataStandAlone(string mealDescription)
        {
            return
                mealDescription.Contains("Salata");
        }

        public static bool IsPuiMeal(string mealDescription)
        {
            return Util.IsInList(KeyWords.PuiList, mealDescription);
        }

        public static bool IsVitaMeal(string mealDescription)
        {
            return Util.IsInList(KeyWords.VitaList, mealDescription);
        }

        public static bool IsPorcMeal(string mealDescription)
        {
            return Util.IsInList(KeyWords.PorcList, mealDescription);
        }

        public static bool IsPesteMeal(string mealDescription)
        {
            return Util.IsInList(KeyWords.PesteList, mealDescription);
        }

        public static bool IsSoiaMeal(string mealDescription)
        {
            return mealDescription.ToLower().Contains("soia");
        }

        public static bool IsPasteMeal(string mealDescription)
        {
            return Util.IsInList(KeyWords.PasteList, mealDescription);
        }

        public static bool IsPartyMeal(string mealDescription)
        {
            return Util.IsInList(KeyWords.PartyMeal, mealDescription);
        }

        public static bool IsSemiPostDay(DateTime dt)
        {
            return dt.DayOfWeek == DayOfWeek.Friday || dt.DayOfWeek == DayOfWeek.Wednesday;
        }

        public static void RemoveMenusAfterSixOclock(this List<UserMenu> userMenus)
        {
            userMenus.RemoveAll(x => Convert.ToDateTime(x.Date).DayOfWeek == DayOfWeek.Wednesday &&
                                     Convert.ToDateTime(x.Date).TimeOfDay >
                                     new DateTime(0001, 01, 01, 18, 00, 0).TimeOfDay);
        }

        public static WeekMenu GetAllWeekMenuItemsByDateOfWeek(this List<WeekMenu> weekMenus, string date)
        {
            return weekMenus.FirstOrDefault(x => x.DayMenus.Any(dayMenu => dayMenu.Date == date));
        }

        public static bool IsNoChoice(string description)
        {
            return description.ToLower() == "-";
        }

        public static bool HasAdditionalSalad(string description)
        {
            return description.Contains("salata de");
        }

        public static bool CheckMeatForApostols(Dictionary<EnumCategories, double> preferences, string description)
        {
            if (IsPorcMeal(description)
                && preferences[EnumCategories.Porcar] < preferences[EnumCategories.Apostol])
                return false;

            if (IsPuiMeal(description)
                && preferences[EnumCategories.Puiar] < preferences[EnumCategories.Apostol])
                return false;

            if (IsVitaMeal(description)
                && preferences[EnumCategories.Vitar] < preferences[EnumCategories.Apostol])
                return false;

            return true;
        }


        public static bool CheckMeatForRegimersType2(Dictionary<EnumCategories, double> preferences, string description,
            DateTime day)
        {
            switch (day.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return CheckRegimType2(preferences, preferences[EnumCategories.RegimarOnMonday], description);
                case DayOfWeek.Tuesday:
                    return CheckRegimType2(preferences, preferences[EnumCategories.RegimarOnTuesday], description);
                case DayOfWeek.Wednesday:
                    return CheckRegimType2(preferences, preferences[EnumCategories.RegimarOnWednesday], description);
                case DayOfWeek.Thursday:
                    return CheckRegimType2(preferences, preferences[EnumCategories.RegimarOnThursday], description);
                case DayOfWeek.Friday:
                    return CheckRegimType2(preferences, preferences[EnumCategories.RegimarOnFriday], description);
            }
            return true;
        }

        private static bool CheckRegimType2(Dictionary<EnumCategories, double> preferences, double percentage,
            string description)
        {
            if (IsPorcMeal(description)
                && preferences[EnumCategories.Porcar] < percentage)
                return false;

            if (IsPuiMeal(description)
                && preferences[EnumCategories.Puiar] < percentage)
                return false;

            if (IsVitaMeal(description)
                && preferences[EnumCategories.Vitar] < percentage)
                return false;
            return true;
        }

        private static bool CheckRegimType1(Dictionary<EnumCategories, double> preferences, double percentage,
            string description)
        {
            if (ContainsMeat(description)
                && preferences[EnumCategories.CiorbarCarnivor] < percentage)
                return false;

            return true;
        }


        public static bool CheckMeatForRegimersType1(Dictionary<EnumCategories, double> preferences, string description,
            DateTime day)
        {
            switch (day.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return CheckRegimType1(preferences, preferences[EnumCategories.RegimarOnMonday], description);
                case DayOfWeek.Tuesday:
                    return CheckRegimType1(preferences, preferences[EnumCategories.RegimarOnTuesday], description);
                case DayOfWeek.Wednesday:
                    return CheckRegimType1(preferences, preferences[EnumCategories.RegimarOnWednesday], description);
                case DayOfWeek.Thursday:
                    return CheckRegimType1(preferences, preferences[EnumCategories.RegimarOnThursday], description);
                case DayOfWeek.Friday:
                    return CheckRegimType1(preferences, preferences[EnumCategories.RegimarOnFriday], description);
            }
            return true;
        }
    }
}