﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CodechefProject.Constants;
using CodechefProject.Models.Menu;
using CodechefProject.Models.User;

namespace CodechefProject.Helpers
{
    public static class Util
    {


        public static string ChangeDateFormat(string date)
        {
            return DateTime.Parse(date).ToString("yyyy-MM-dd");
        }

        public static string ChangeDateFormat(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd");
        }

        public static List<UserMenu> ChangedDateFormat(List<UserMenu> userMenus)
        {
            foreach (var userMenu in userMenus)
            {
                userMenu.Date = DateTime.Parse(userMenu.Date).ToString("yyyy-MM-dd");
                userMenu.Day = DateTime.Parse(userMenu.Day).ToString("yyyy-MM-dd");
            }
            return userMenus;
        }

        public static WeekMenu ChangedDateFormat(this WeekMenu weekMenu)
        {
            foreach (var item in weekMenu.DayMenus)
                item.Date = DateTime.Parse(item.Date).ToString("yyyy-MM-dd");
            weekMenu.Monday = DateTime.Parse(weekMenu.Monday).ToString("yyyy-MM-dd");

            return weekMenu;
        }


        public static List<WeekMenu> ChangedDateFormat(this List<WeekMenu> weekMenus)
        {
            foreach (var weekMenu in weekMenus)
                if (weekMenu != null)
                {
                    foreach (var item in weekMenu.DayMenus)
                        item.Date = DateTime.Parse(item.Date).ToString("yyyy-MM-dd");

                    weekMenu.Monday = DateTime.Parse(weekMenu.Monday).ToString("yyyy-MM-dd");
                }
            return weekMenus;
        }

        public static string RemoveCodeFromDescription(this string description)
        {
            var index = description.IndexOf('.');
            if (index >= 0)
                return description.Substring(index + 3);
            return description;
        }

        public static List<string> RemoveCodeFromDescriptionInList(this List<string> list)
        {
            return list.Select(RemoveCodeFromDescription).ToList();
        }

        public static bool IsInList(List<string> keyWords, string searchedText)
        {
            return keyWords.Any(s => searchedText.ToLower().Contains(s));
        }

        public static void LogInFile(string filePath, string s)
        {
            using (var sw = File.AppendText(filePath))
            {
                sw.WriteLine(s.ToLower());
            }
        }

        public static double LessWithPercent(this double value, double percent)
        {
            return value - value * percent;
        }

        public static double GetPercent(DateTime date)
        {
            var endDate = ConstantValues.EndDate;
            var months = (endDate.Year - date.Year) * 12 + endDate.Month - date.Month;

            double procent = 1;

            for (var i = months; i >= 1; i--)
                procent = procent - ConstantValues.PERCENT_STEP * procent;
            return procent;
        }

        public static double GetPercent(string menuDate)
        {
            var currentDate = DateTime.Parse(menuDate);
            return GetPercent(currentDate);
        }
    }
}