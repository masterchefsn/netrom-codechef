﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using CodechefProject.Models.User;

namespace CodechefProject.Helpers
{
    public class UserXMLWriter
    {
        public static void WriteToFile(string filePath, List<User> users)
        {
            Regex pattern = new Regex(" ,.;");


            var xdoc = new XDocument(new XElement("Users",
                    from user in users
                    select
                    new XElement("User",
                        new XAttribute("UID", user.UID),
                        new XAttribute("Name", user.Name),
                        new XAttribute("Gender", user.Gender),
                        new XAttribute("Age", user.Age),
                        new XElement("Percentages",
                            new XElement("Type1Percentages",
                                from p1 in user.dictionary1
                                select
                                new XElement(pattern.Replace(p1.Key.ToString(),"I"), p1.Value)),
                            new XElement("Type2Percentages",
                                from p2 in user.dictionary2
                                select
                                new XElement(pattern.Replace(p2.Key.ToString(),"I"), p2.Value))),
                        new XElement("AbsoluteValues",
                            from a in user.dictionaryAbsoluteValues
                            select
                            new XElement(pattern.Replace(a.Key.ToString(), "I"), a.Value)),
                        new XElement("FavPorcRecipies",
                            from rec in user.FavoritePorcIngredients
                            select
                            new XElement("aaa1", rec.Value)
                        )
                    )
                )
            );

            xdoc.Save(filePath);
        }
    }
}