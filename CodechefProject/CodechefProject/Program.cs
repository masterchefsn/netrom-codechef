﻿using System;
using CodechefProject.Constants;
using CodechefProject.Helpers;
using CodechefProject.Models.User;
using CodechefProject.Service;
using CodechefProject.UserManagement;

namespace CodechefProject
{
    internal class Program
    {
        private static readonly UsersService usersService = new UsersService();
        private static readonly ReceipeService recipeService = new ReceipeService();
        private static AppCategories _appCategories;
        private static MenuService menuService = new MenuService();

        private static void Main(string[] args)
        {
            CreateUserMenus();
        }

        private static void CreateUserMenus()
        {
            var users = usersService.GetAllUsers();
            GlobalVariables.Receipes = recipeService.GetAllRecipes();

            _appCategories = new AppCategories();
            menuService = new MenuService();

            var weekMenus = menuService.ComputeCategories(ConstantValues.StartDate, ConstantValues.EndDate, _appCategories);
            var upb = new UsersProfileBuilder();
            var ucs = new UserChoiceSelector();

            upb.CreateUsersProfile(users, weekMenus);

            _appCategories.HandleAllMissingCategories();

            foreach (var user in users)
            {
                user.Compute(_appCategories);
            }
            UserXMLWriter.WriteToFile("D://userprofiles2.xml", users);

            ucs.SelectChoice(users);

            
        }
    }
}