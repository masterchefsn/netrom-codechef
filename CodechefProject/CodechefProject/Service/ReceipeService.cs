﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using CodechefProject.Constants;
using CodechefProject.Models.Receipe;
using Newtonsoft.Json;

namespace CodechefProject.Service
{
    public class ReceipeService
    {
        public List<Receipe> GetAllRecipes()
        {
            string uri = ConstantValues.baseapi + "/recipes";
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
            webrequest.Method = "GET";
            webrequest.ContentType = "application/x-www-form-urlencoded";
            HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
            Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);
            string result = string.Empty;
            result = responseStream.ReadToEnd();
            webresponse.Close();

            return JsonConvert.DeserializeObject<List<Receipe>>(result);
        }
    }
}
