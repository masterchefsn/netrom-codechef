﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CodechefProject.Constants;
using CodechefProject.Models.User;
using Newtonsoft.Json;

namespace CodechefProject.Service
{
    public class UsersService
    {
        //public static string baseapi = "http://codechefapi.stage02.netromsoftware.ro/api/YfE6qGCKNHpAaSIXJrzr";
        public List<User> GetAllUsers() 
        {
            
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(ConstantValues.baseapi + "/users");
            webrequest.Method = "GET";
            webrequest.ContentType = "application/x-www-form-urlencoded";
            webrequest.Headers.Add("Username", "xyz");
            webrequest.Headers.Add("Password", "abc");
            HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
            Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);
            string result = string.Empty;
            result = responseStream.ReadToEnd();
            webresponse.Close();

            return JsonConvert.DeserializeObject<List<User>>(result);
        }

        public List<User> GetAllUsersByID(string uid)
        {
            string uri = ConstantValues.baseapi  + "/users/" + uid ;
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
            webrequest.Method = "GET";
            webrequest.ContentType = "application/x-www-form-urlencoded";
            webrequest.Headers.Add("Username", "xyz");
            webrequest.Headers.Add("Password", "abc");
            HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
            Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);
            string result = string.Empty;
            result = responseStream.ReadToEnd();
            webresponse.Close();

            return JsonConvert.DeserializeObject<List<User>>(result);
        }

        public async Task<string> UpdateUser(string UID, List<UserMenu> menus)
        {
            string uri = ConstantValues.baseapi + "/usermenu/" + UID;
            HttpClient client = new HttpClient();
            HttpRequestMessage message = new HttpRequestMessage(new HttpMethod("POST"), uri);

            List<UserMenu> userMenus = new List<UserMenu>();


            var json = JsonConvert.SerializeObject(menus);
            message.Content = new StringContent(json,
                                    null,
                                    "application/json");//CONTENT-TYPE header

            message.Content.Headers.Remove("Content-Type");
            message.Content.Headers.Add("Content-Type", "application/json");

            try
            {
                HttpResponseMessage httpResponseMessage = await client.SendAsync(message);
                httpResponseMessage.EnsureSuccessStatusCode();
                HttpContent httpContent = httpResponseMessage.Content;
                string responseString = await httpContent.ReadAsStringAsync();
                System.Threading.Thread.Sleep(50);
                return responseString;
            }
            catch (Exception ex)
            {
                string errorType = ex.GetType().ToString();
                string errorMessage = errorType + ": " + ex.Message;
                throw new Exception(errorMessage, ex.InnerException);
            }
        }
    }
}
