﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using CodechefProject.CategoryManagement;
using CodechefProject.Constants;
using CodechefProject.Helpers;
using CodechefProject.Models.Menu;
using CodechefProject.Models.User;
using Newtonsoft.Json;

namespace CodechefProject.Service
{
    public class MenuService
    {
        public WeekMenu GetAllWeekMenuItemsByDateOfWeek(string dateOfWeek)
        {
            try
            {
                string uri = ConstantValues.baseapi + "/weekmenu/" + dateOfWeek;
                HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
                webrequest.Method = "GET";
                webrequest.ContentType = "application/x-www-form-urlencoded";
                HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
                Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);
                string result = string.Empty;
                result = responseStream.ReadToEnd();
                webresponse.Close();
                return JsonConvert.DeserializeObject<WeekMenu>(result);
            }

            catch (Exception)
            {
                return null;

            }
        }

        public List<UserMenu> GetAllUserMenusByStartEndDate(string startDate, string endDate, string userId)
        {
            string uri = ConstantValues.baseapi + "/usermenu/" + userId + "/" + startDate + "/" + endDate;
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
            webrequest.Method = "GET";
            webrequest.ContentType = "application/x-www-form-urlencoded";
            HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
            Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);
            string result = string.Empty;
            result = responseStream.ReadToEnd();
            webresponse.Close();

            return JsonConvert.DeserializeObject<List<UserMenu>>(result);
        }

        public List<UserMenu> SaveUserMenu(string userId)
        {
            string uri = ConstantValues.baseapi + "/usermenu/" + userId;
            HttpWebRequest webrequest = (HttpWebRequest)WebRequest.Create(uri);
            webrequest.Method = "POST";
            webrequest.ContentType = "application/x-www-form-urlencoded";
            HttpWebResponse webresponse = (HttpWebResponse)webrequest.GetResponse();
            Encoding enc = System.Text.Encoding.GetEncoding("utf-8");
            StreamReader responseStream = new StreamReader(webresponse.GetResponseStream(), enc);
            string result = string.Empty;
            result = responseStream.ReadToEnd();
            webresponse.Close();


            return JsonConvert.DeserializeObject<List<UserMenu>>(result);
        }

        private List<WeekMenu> GetAllWeekMenuFromStartDateToEndDate(DateTime startDate, DateTime endDate)
        {
            List<WeekMenu> weekMenus = new List<WeekMenu>();
            while (startDate < endDate)
            {
                var currentWeekMenu = GetAllWeekMenuItemsByDateOfWeek(startDate.ToString("yyyy-MM-dd"));
                startDate = startDate.AddDays(7);
                weekMenus.Add(currentWeekMenu);
            }

            return weekMenus;
        }

        public List<WeekMenu> ComputeCategories(DateTime startDate, DateTime endDate, AppCategories appCategories)
        {
            List<WeekMenu> weekMenus = GetAllWeekMenuFromStartDateToEndDate(startDate, endDate);

            CategoryBuilder cb = new CategoryBuilder();
            foreach (var weekMenu in weekMenus)
            {
                if (weekMenu?.DayMenus != null)
                {
                    foreach (var dayMenu in weekMenu?.DayMenus)
                    {
                        cb.ComputeTotalChoices(dayMenu, appCategories);
                    }
                }
            }
            weekMenus.RemoveAll(x => x == null);
            weekMenus = weekMenus.ChangedDateFormat();
            return weekMenus;
        }
    }
}
