﻿namespace CodechefProject.Models
{
    public class AbsolutePercentage
    {
        public double Percentage { get; set; }
        public int AbsoluteValue { get; set; }
    }
}