﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodechefProject.Models.User
{
    public class MenusDaysOfWeek
    {
        public MenusDaysOfWeek(List<UserMenu> userMenus)
        {
            MondayMenus = userMenus.Count(x => Convert.ToDateTime(x.Day).DayOfWeek == DayOfWeek.Monday);
            TuesdayMenus = userMenus.Count(x => Convert.ToDateTime(x.Day).DayOfWeek == DayOfWeek.Tuesday);
            WednesdayMenus = userMenus.Count(x => Convert.ToDateTime(x.Day).DayOfWeek == DayOfWeek.Wednesday);
            ThursdayMenus = userMenus.Count(x => Convert.ToDateTime(x.Day).DayOfWeek == DayOfWeek.Thursday);
            FridayMenus = userMenus.Count(x => Convert.ToDateTime(x.Day).DayOfWeek == DayOfWeek.Friday);

            TotalMenus = userMenus.Count;
        }

        public int MondayMenus { get; set; }
        public int TuesdayMenus { get; set; }
        public int WednesdayMenus { get; set; }
        public int ThursdayMenus { get; set; }
        public int FridayMenus { get; set; }

        public int TotalMenus { get; set; }

        public Dictionary<string, double> GetPercentages(Dictionary<DayOfWeek, double> appearances)
        {
            var percDeic = new Dictionary<string, double>();
            foreach (var item in appearances)
            {
                var percentage = (double) item.Value / GetAppearancesForDayOfWeek(item.Key);
                percDeic.Add(item.Key.ToString(), percentage);
            }

            return percDeic;
        }

        public void UpdateTotalNumber(string day)
        {
            var dow = Convert.ToDateTime(day).DayOfWeek;
            switch (dow)
            {
                case DayOfWeek.Monday:
                    MondayMenus++;
                    break;
                case DayOfWeek.Tuesday:
                    TuesdayMenus++;
                    break;
                case DayOfWeek.Wednesday:
                    WednesdayMenus++;
                    break;
                case DayOfWeek.Thursday:
                    ThursdayMenus++;
                    break;
                case DayOfWeek.Friday:
                    FridayMenus++;
                    break;
            }
        }

        private int GetAppearancesForDayOfWeek(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return MondayMenus!= 0? MondayMenus: Constants.ConstantValues.MIE;
                case DayOfWeek.Tuesday:
                    return TuesdayMenus != 0 ? TuesdayMenus : Constants.ConstantValues.MIE;
                case DayOfWeek.Wednesday:
                    return WednesdayMenus != 0 ? WednesdayMenus : Constants.ConstantValues.MIE;
                case DayOfWeek.Thursday:
                    return ThursdayMenus != 0 ? ThursdayMenus : Constants.ConstantValues.MIE;
                case DayOfWeek.Friday:
                    return FridayMenus != 0 ? FridayMenus : Constants.ConstantValues.MIE;
            }
            return TotalMenus;
        }
    }
}