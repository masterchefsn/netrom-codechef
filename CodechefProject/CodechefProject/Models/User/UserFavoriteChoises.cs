﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodechefProject.Models.User
{
    public class UserFavoriteChoises
    {
        public UserFavoriteChoises() 
        {
            SelectedCount = 0;
            Ingredients = new List<string>();
            Apparences = 0;
        }

        public string FoodDescription { get; set; }
        public int SelectedCount { get; set; }
        public List<string> Ingredients { get; set; }
        public int Apparences { get; set; }
        public string Type { get; set; }
        public double Percentage { get; set; }
    }
}
