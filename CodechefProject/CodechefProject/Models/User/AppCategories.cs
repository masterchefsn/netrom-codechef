﻿using System;
using System.Collections.Generic;
using CodechefProject.Constants;

namespace CodechefProject.Models.User
{
    public class AppCategories
    {
        public AppCategories()
        {
            Categories = new List<MonthCategoryContainer>();
            var startDate = new DateTime(ConstantValues.StartDate.Year, ConstantValues.StartDate.Month, 1);  
            var endDate = ConstantValues.EndDate;

            while (endDate >= startDate || (endDate.Month == startDate.Month && endDate.Year == startDate.Year))
            {
                var category = new MonthCategoryContainer();
                category.EndDate = endDate.Date;
                category.StartDate = endDate.AddDays(Constants.ConstantValues.DAYS_STEP).AddDays(1);
                //category.StartDate = endDate.AddMonths(-1).AddDays(1);

                Categories.Add(category);
               // endDate = endDate.AddDays(Constants.ConstantValues.DAYS_STEP);
                endDate = endDate.AddMonths(-1);
            }
        }

        public List<MonthCategoryContainer> Categories { get; set; }

        public void HandleAllMissingCategories()
        {
            foreach (var category in Categories)
                category.HandleMissingCategoriesInContainer();
        }
    }

    public enum EnumCategories
    {
        Carnar,
        Puiar,
        Porcar,
        Vitar,
        Pescar,
        Macaronar,
        Salatar,
        SoiaLover,
        Apostol,
        //Regimar,
        NoNo2,
        NoNo1,
        CiorbarCarnivor,
        CiorbarErbivor,
        Supar,
        Prajiturar,
        Fructar,

        RegimarOnMonday,
        RegimarOnTuesday,
        RegimarOnWednesday,
        RegimarOnThursday,
        RegimarOnFriday
    }
}