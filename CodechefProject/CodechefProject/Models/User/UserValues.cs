﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodechefProject.Constants;
using CodechefProject.Helpers;

namespace CodechefProject.Models.User
{
    public class UserValues
    {
        public Dictionary<EnumCategories, double> dictionary1 = new Dictionary<EnumCategories, double>();
        public Dictionary<EnumCategories, double> dictionary2 = new Dictionary<EnumCategories, double>();
        public Dictionary<EnumCategories, double> dictionaryAbsoluteValues = new Dictionary<EnumCategories, double>();
        
        public MenusDaysOfWeek MenusOnDays { get; set; }

        public Dictionary<DayOfWeek, double> RegimNumberOnDay { get; set; } = new Dictionary<DayOfWeek, double>();
        public Dictionary<string, double> RegimPercentageOnDay { get; set; } = new Dictionary<string, double>();

        public DateTime EndDate { get; set; }

        public double percentageCiorbarCarnivor;
        public double percentageCiorbarErbivor;
        public double percentageFructar;
        public double percentageMacaronar;
        public double percentageNoNo1;
        public double percentageNono2;
        public double percentagePescar;
        public double percentagePorcar;
        public double percentagePrajiturar;

        public double percentagePuiar;
        public double percentageSalatar;
        public double percentageSupar;
        public double percentageVitar;
        public double percentageSoiaLover;

        public double CiorbarCarnivor { get; set; }
        public double CiorbarErbivor { get; set; }
        public double Prajiturar { get; set; }
        public double Fructar { get; set; }
        public double NoNo1 { get; set; }
        public double Cremos { get; set; }

        public double Apostol { get; set; }
        public double Regim { get; set; }
        public double Puiar { get; set; }
        public double Porcar { get; set; }
        public double Vitar { get; set; }
        public double Salatar { get; set; }
        public double Pescar { get; set; }
        public double SoiaLover { get; set; }
        public double NoNo2 { get; set; }
        public double Macaronar { get; set; }

        public UserValues()
        {
            this.InitializeRegimDays();
        }

        public double ComputationPercentage => Util.GetPercent(this.EndDate);
        public DateTime StartDate { get; set; }
        public double PercentageSet { get; set; }

        public void Compute(MonthCategoryContainer monthCategoryContainer)
        {
            RegimPercentageOnDay = MenusOnDays.GetPercentages(RegimNumberOnDay);
            AddRegimCategories();

            this.ComputeType1Percentage(monthCategoryContainer);
            this.ComputeType2Percentage(monthCategoryContainer);
            this.CreateAbsoluteValueDictionary(monthCategoryContainer);
        }

        private void ComputeType1Percentage(MonthCategoryContainer monthCategoryContainer)
        {
            //fel 1
            percentageCiorbarCarnivor = (double)CiorbarCarnivor / monthCategoryContainer.NrCiorbeCarne;
            percentageCiorbarErbivor = (double)CiorbarErbivor / monthCategoryContainer.NrCiorbeFaraCarne;
            percentageSupar = (double)Cremos / monthCategoryContainer.NrCreme;
            percentagePrajiturar = (double)Prajiturar / monthCategoryContainer.NrPrajituri;
            percentageFructar = (double)Fructar / monthCategoryContainer.NrFructe;
            percentageNoNo1 = (double)NoNo1 / MenusOnDays.TotalMenus;

            dictionary1.Add(EnumCategories.CiorbarCarnivor, percentageCiorbarCarnivor);
            dictionary1.Add(EnumCategories.CiorbarErbivor, percentageCiorbarErbivor);
            dictionary1.Add(EnumCategories.Supar, percentageSupar);
            dictionary1.Add(EnumCategories.Prajiturar, percentagePrajiturar);
            dictionary1.Add(EnumCategories.Fructar, percentageFructar);
            dictionary1.Add(EnumCategories.NoNo1, percentageNoNo1);

            dictionary1 = dictionary1.OrderByDescending(x => x.Value).
                ToDictionary(x => x.Key, x => x.Value);
        }
        
        private void ComputeType2Percentage(MonthCategoryContainer monthCategoryContainer)
        {
            RegimPercentageOnDay = MenusOnDays.GetPercentages(RegimNumberOnDay);
            //fel 2
            percentagePuiar = (double)Puiar / monthCategoryContainer.NrPui;
            percentagePorcar = (double)Porcar / monthCategoryContainer.NrPorci;
            percentageSalatar = (double)Salatar / monthCategoryContainer.NrSalate; //monthCategoryContainer.NrSalate;
            percentagePescar = (double)Pescar / monthCategoryContainer.NrPesti;
            percentageMacaronar = (double)Macaronar / monthCategoryContainer.NrMacaroane;
            percentageNono2 = (double)NoNo2 / MenusOnDays.TotalMenus;
            percentageVitar = (double)Vitar / monthCategoryContainer.NrVita;
            percentageSoiaLover = (double)SoiaLover / monthCategoryContainer.NrSoia;

            var percentageApostol = (double)Apostol / monthCategoryContainer.NrPost;

            dictionary2.Add(EnumCategories.Salatar, percentageSalatar);
            dictionary2.Add(EnumCategories.Pescar, percentagePescar);
            dictionary2.Add(EnumCategories.Vitar, percentageVitar);
            dictionary2.Add(EnumCategories.Macaronar, percentageMacaronar);
            dictionary2.Add(EnumCategories.NoNo2, percentageNono2);
            dictionary2.Add(EnumCategories.Apostol,0);// percentageApostol);
            dictionary2.Add(EnumCategories.SoiaLover, percentageSoiaLover);

            dictionary1.Add(EnumCategories.Apostol, percentageApostol);

            if (Math.Abs(percentagePuiar - percentagePorcar) < ConstantValues.PuiPorcAllowedDifference)
            {
                var percentagePuiPorc = (double)(Puiar + Porcar + Vitar) / (monthCategoryContainer.NrPui
                                                                             + monthCategoryContainer.NrPorci + monthCategoryContainer.NrVita);
                //var percentagePuiPorc = (double) percentagePuiar + percentagePorcar;

                dictionary2.Add(EnumCategories.Carnar, percentagePuiPorc);
                dictionary2.Add(EnumCategories.Puiar, percentagePuiar);
                dictionary2.Add(EnumCategories.Porcar, percentagePorcar);
            }
            else
            {
                dictionary2.Add(EnumCategories.Puiar, percentagePuiar);
                dictionary2.Add(EnumCategories.Porcar, percentagePorcar);
            }

            dictionary2 =
                dictionary2.OrderByDescending(x => x.Value)
                    .ToDictionary(x => x.Key, x => x.Value);
        }
        
        private void CreateAbsoluteValueDictionary(MonthCategoryContainer monthCategoryContainer)
        {
            dictionaryAbsoluteValues.Add(EnumCategories.CiorbarCarnivor, CiorbarCarnivor);
            dictionaryAbsoluteValues.Add(EnumCategories.CiorbarErbivor, CiorbarErbivor);
            dictionaryAbsoluteValues.Add(EnumCategories.Supar, Cremos);
            dictionaryAbsoluteValues.Add(EnumCategories.Prajiturar, Prajiturar);
            dictionaryAbsoluteValues.Add(EnumCategories.Fructar, Fructar);
            dictionaryAbsoluteValues.Add(EnumCategories.NoNo1, NoNo1);

            dictionaryAbsoluteValues.Add(EnumCategories.Puiar, Puiar);
            dictionaryAbsoluteValues.Add(EnumCategories.Porcar, Porcar);
            dictionaryAbsoluteValues.Add(EnumCategories.Salatar, Salatar);
            dictionaryAbsoluteValues.Add(EnumCategories.Pescar, Pescar);
            dictionaryAbsoluteValues.Add(EnumCategories.Macaronar, Macaronar);
            dictionaryAbsoluteValues.Add(EnumCategories.NoNo2, NoNo2);
            dictionaryAbsoluteValues.Add(EnumCategories.Apostol, 0);//Apostol);
            dictionaryAbsoluteValues.Add(EnumCategories.Vitar, Vitar);
            dictionaryAbsoluteValues.Add(EnumCategories.SoiaLover, SoiaLover);
            dictionaryAbsoluteValues.Add(EnumCategories.Carnar, Puiar + Porcar);

            dictionaryAbsoluteValues.Add(EnumCategories.RegimarOnMonday, 0);//GetRegimarOnDayAbsoluteValue(EnumCategories.RegimarOnMonday));
            dictionaryAbsoluteValues.Add(EnumCategories.RegimarOnTuesday, 0);//GetRegimarOnDayAbsoluteValue(EnumCategories.RegimarOnTuesday));
            dictionaryAbsoluteValues.Add(EnumCategories.RegimarOnWednesday, 0);//GetRegimarOnDayAbsoluteValue(EnumCategories.RegimarOnWednesday));
            dictionaryAbsoluteValues.Add(EnumCategories.RegimarOnThursday, 0);//GetRegimarOnDayAbsoluteValue(EnumCategories.RegimarOnThursday));
            dictionaryAbsoluteValues.Add(EnumCategories.RegimarOnFriday, 0);//GetRegimarOnDayAbsoluteValue(EnumCategories.RegimarOnFriday));
            //dictionaryAbsoluteValues.Add(EnumCategories.Regimar, Regim);

            dictionaryAbsoluteValues = dictionaryAbsoluteValues.OrderByDescending(x => x.Value)
                .ToDictionary(x => x.Key, x => x.Value);
        }


        private void InitializeRegimDays()
        {
            RegimNumberOnDay.Add(DayOfWeek.Monday, 0);
            RegimNumberOnDay.Add(DayOfWeek.Tuesday, 0);
            RegimNumberOnDay.Add(DayOfWeek.Wednesday, 0);
            RegimNumberOnDay.Add(DayOfWeek.Thursday, 0);
            RegimNumberOnDay.Add(DayOfWeek.Friday, 0);
        }

        public double GetRegimarOnDayAbsoluteValue(EnumCategories category)
        {
            switch (category)
            {
                case EnumCategories.RegimarOnMonday:
                    return RegimNumberOnDay[DayOfWeek.Monday];
                case EnumCategories.RegimarOnTuesday:
                    return RegimNumberOnDay[DayOfWeek.Tuesday];
                case EnumCategories.RegimarOnWednesday:
                    return RegimNumberOnDay[DayOfWeek.Wednesday];
                case EnumCategories.RegimarOnThursday:
                    return RegimNumberOnDay[DayOfWeek.Thursday];
                case EnumCategories.RegimarOnFriday:
                    return RegimNumberOnDay[DayOfWeek.Friday];
            }
            return 0;
        }

        private void AddRegimCategories()
        {
            dictionary2.Add(EnumCategories.RegimarOnMonday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[0]]);
            dictionary2.Add(EnumCategories.RegimarOnTuesday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[1]]);
            dictionary2.Add(EnumCategories.RegimarOnWednesday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[2]]);
            dictionary2.Add(EnumCategories.RegimarOnThursday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[3]]);
            dictionary2.Add(EnumCategories.RegimarOnFriday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[4]]);

            dictionary1.Add(EnumCategories.RegimarOnMonday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[0]]);
            dictionary1.Add(EnumCategories.RegimarOnTuesday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[1]]);
            dictionary1.Add(EnumCategories.RegimarOnWednesday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[2]]);
            dictionary1.Add(EnumCategories.RegimarOnThursday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[3]]);
            dictionary1.Add(EnumCategories.RegimarOnFriday, 0);//RegimPercentageOnDay[ConstantValues.DayNames[4]]);
        }
    }
}
