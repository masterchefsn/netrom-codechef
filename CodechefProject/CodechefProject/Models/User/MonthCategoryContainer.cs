﻿using System;
using CodechefProject.Constants;

namespace CodechefProject.Models.User
{
    public class MonthCategoryContainer
    {
        public double NrCiorbeCarne;
        public double NrCiorbeFaraCarne;
        public double NrCreme;
        public double NrFructe;
        public double NrMacaroane;

        public double NrPesti;
        public double NrPorci;
        public double NrPost;
        public double NrPrajituri;
        public double NrPui;
        public double NrSalate;
        public double NrSoia;
        public double NrVita;

        public double OtherNo1 = 0;
        public decimal OtherNo2 = 0;

        public DateTime EndDate { get; set; }
        public DateTime StartDate { get; set; }

        //public int MenusOnDays = 0;

        public void HandleMissingCategoriesInContainer()
        {
            if (NrPui == 0)
                NrPui = ConstantValues.MIE;

            if (NrPorci == 0)
                NrPorci = ConstantValues.MIE;

            if (NrPesti == 0)
                NrPesti = ConstantValues.MIE;

            if (NrMacaroane == 0)
                NrMacaroane = ConstantValues.MIE;

            if (NrSalate == 0)
                NrSalate = ConstantValues.MIE;

            if (NrSoia == 0)
                NrSoia = ConstantValues.MIE;

            if (NrPost == 0)
                NrPost = ConstantValues.MIE;

            if (NrVita == 0)
                NrVita = ConstantValues.MIE;

            if (NrCiorbeCarne == 0)
                NrCiorbeCarne = ConstantValues.MIE;

            if (NrCiorbeFaraCarne == 0)
                NrCiorbeFaraCarne = ConstantValues.MIE;

            if (NrCreme == 0)
                NrCreme = ConstantValues.MIE;
            if (NrPrajituri == 0)
                NrPrajituri = ConstantValues.MIE;
            if (NrFructe == 0)
                NrFructe = ConstantValues.MIE;
        }
    }
}