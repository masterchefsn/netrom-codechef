﻿namespace CodechefProject.Models.User
{
    public class UserFavoriteIngredient
    {
        public UserFavoriteIngredient()
        {
            SelectedCount = 0;
            Apparences = 1;
        }

        public string IngredientDescription { get; set; }
        public int SelectedCount { get; set; }
        public int Apparences { get; set; }

        public double Percentage =>
        (double) SelectedCount / Apparences;
    }
}