﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodechefProject.Constants;
using CodechefProject.Models.Menu;

namespace CodechefProject.Models.User
{
    public class User
    {
        public Dictionary<EnumCategories, double> dictionary1 = new Dictionary<EnumCategories, double>();
        public Dictionary<EnumCategories, double> dictionary2 = new Dictionary<EnumCategories, double>();
        public Dictionary<EnumCategories, double> dictionaryAbsoluteValues = new Dictionary<EnumCategories, double>();
        public List<UserFavoriteChoises> FavoriteChoises = new List<UserFavoriteChoises>();
        public List<UserFavoriteChoises> FavoriteChoisesForType1 = new List<UserFavoriteChoises>();
        public List<UserFavoriteChoises> FavoriteChoisesForType2 = new List<UserFavoriteChoises>();

        public Dictionary<string, int> FavoritePorcIngredients = new Dictionary<string, int>();

        public Dictionary<string, double> FavoritePorcPercentages = new Dictionary<string, double>();
        public Dictionary<string, int> FavoritePuiIngredients = new Dictionary<string, int>();
        public Dictionary<string, double> FavoritePuiPercentages = new Dictionary<string, double>();
        public Dictionary<string, int> FavoriteVitaIngredients = new Dictionary<string, int>();
        public Dictionary<string, double> FavoriteVitaPercentages = new Dictionary<string, double>();
        public List<UserFavoriteChoises> HatedChoisesForType1 = new List<UserFavoriteChoises>();

        public List<UserFavoriteChoises> HatedChoisesForType2 = new List<UserFavoriteChoises>();


        public Dictionary<string, double> PuiAndPorcAndVitaPercentages = new Dictionary<string, double>();

        public Dictionary<string, double> PuiAndPorcPercentages = new Dictionary<string, double>();

        public List<UserFavoriteIngredient> UserHatedIngredients = new List<UserFavoriteIngredient>();

        public List<UserFavoriteIngredient> UserFavoriteIngredients = new List<UserFavoriteIngredient>();

        public List<UserFavoriteIngredient> UserIngredients = new List<UserFavoriteIngredient>();

        public User()
        {
            InitialzeUserValues();
            //InitializeRegimDays();
        }

        public List<UserValues> UserValues { get; set; } = new List<UserValues>();


        //public Dictionary<DayOfWeek, double> RegimNumberOnDay { get; set; } = new Dictionary<DayOfWeek, double>();
        //public Dictionary<string, double> RegimPercentageOnDay { get; set; } = new Dictionary<string, double>();

        public string UID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public int Age { get; set; }


        public int ExtraLover { get; set; } = 0;

        public WeekMenu WeekMenu { get; set; }

        public double PercentageSoiaLover { get; set; }

        private void InitialzeUserValues()
        {
            var startDate = new DateTime(ConstantValues.StartDate.Year, ConstantValues.StartDate.Month, 1);
            var endDate = ConstantValues.EndDate;

            while (endDate >= startDate || endDate.Month == startDate.Month && endDate.Year == startDate.Year)
            {
                var userValue = new UserValues();
                userValue.EndDate = endDate.Date;
                userValue.StartDate = endDate.AddDays(ConstantValues.DAYS_STEP).AddDays(1);
                //userValue.StartDate = endDate.AddMonths(-1).AddDays(1);

                UserValues.Add(userValue);
                //endDate = endDate.AddDays(ConstantValues.DAYS_STEP);
                endDate = endDate.AddMonths(-1);
            }
        }

        public UserValues GetUserValues(DateTime date)
        {
            //return UserValues.FirstOrDefault(x => x.EndDate >= date && x.StartDate <= date);
            return UserValues.FirstOrDefault(x => x.EndDate.Month == date.Month && x.EndDate.Year == date.Year);
        }

        public void ComputeTotalNumberOfMenus(List<UserMenu> userMenus)
        {
            foreach (var item in UserValues)
                item.MenusOnDays =
                    new MenusDaysOfWeek(
                        //userMenus.Where(um => Convert.ToDateTime(um.Day) <= item.EndDate
                        //                      && Convert.ToDateTime(um.Day) >= item.StartDate
                        userMenus.Where(um => Convert.ToDateTime(um.Day).Year == item.EndDate.Year
                                              && Convert.ToDateTime(um.Day).Month == item.EndDate.Month
                            )
                            .ToList());
        }

        private void SetPercentages()
        {
            var count = UserValues.Count;
            var division = (double) 1 / count;

            var step = 0.70;

            int i = 0, j = count - 1;

            foreach (var value in UserValues)
                value.PercentageSet = division;

            while (i < j)
            {
                UserValues[i].PercentageSet += step * division;
                UserValues[j].PercentageSet -= step * division;

                step = step / 2;
                i++;
                j--;
            }
        }

        public void Compute(AppCategories monthCategoryContainer)
        {
            ComputeMonthValues(monthCategoryContainer);
            SetPercentages();
            ComputeType1TotalPercentage();
            ComputeType2TotalPercentage();
            ComputeAbsoluteTotalPercentage();

            FavoritePorcIngredients =
                FavoritePorcIngredients.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

            FavoritePuiIngredients =
                FavoritePuiIngredients.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

            FavoriteVitaIngredients =
                FavoriteVitaIngredients.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

            CreatePorcPercentageDictionary();
            CreatePuiPercentageDictionary();
            CreateVitaPercentageDictionary();

            MergePorcAndPui();
            MergePorcAndPuiAndVita();

            ComputeFavoriteChoicesPercentages();
            //AddRegimCategories();

            dictionary1 = dictionary1.OrderByDescending(x => x.Value).
                ThenByDescending(x => dictionaryAbsoluteValues[x.Key]).ToDictionary(x => x.Key, x => x.Value);
            dictionary2 = dictionary2.OrderByDescending(x => x.Value).
                ThenByDescending(x => dictionaryAbsoluteValues[x.Key]).ToDictionary(x => x.Key, x => x.Value);
            this.ComputeHatedIngredientPercentages();
        }


        private void ComputeHatedIngredientPercentages()
        {
            UserIngredients = UserIngredients.OrderBy(x => x.Apparences).ToList();

            var maxDif = 0;
            var i = 0;
            var n = UserIngredients.Count;
            var index = n;

            while (i < n - 2)
            {
                if (UserIngredients[i + 1].Apparences - UserIngredients[i].Apparences > maxDif)
                {
                    maxDif = UserIngredients[i + 1].Apparences - UserIngredients[i].Apparences;
                    index = i;
                }
                i++;
            }

            //var lenght = n - index - 1;
            //UserIngredients.RemoveRange(index + 1, lenght);

            UserHatedIngredients =
                UserIngredients.Where(x => x.Percentage < ConstantValues.HATED_INGREDIENT_PERCENT
                && x.Apparences < 50).ToList();

            UserFavoriteIngredients =
                UserIngredients.Where(x => x.Percentage > ConstantValues.PERCENTAGE_MARGIN
                && x.Apparences > 2).ToList();
        }

        private void ComputeMonthValues(AppCategories appCategoryContainer)
        {
            foreach (var item in UserValues)
                item.Compute(appCategoryContainer.Categories.FirstOrDefault(x => x.EndDate == item.EndDate &&
                                                                                 x.StartDate == item.StartDate));
        }

        private void ComputeType1TotalPercentage()
        {
            double percentage = 1;
            foreach (var userValue in UserValues)
            {
                var values = userValue.dictionary1;
                //percentage = percentage.LessWithPercent(ConstantValues.PERCENT_STEP);
                // percentage = userValue.ComputationPercentage;
                percentage = percentage - 0.5 * percentage;

                foreach (var item in values)
                    if (!dictionary1.ContainsKey(item.Key))
                        dictionary1.Add(item.Key, item.Value * /*percentage*/ userValue.PercentageSet);
                    else
                        dictionary1[item.Key] += userValue.PercentageSet /*percentage*/* item.Value;
            }
        }

        private void ComputeType2TotalPercentage()
        {
            double percentage = 1;
            foreach (var userValue in UserValues)
            {
                var values = userValue.dictionary2;
                //var percentage = userValue.ComputationPercentage;
                //percentage = percentage.LessWithPercent(ConstantValues.PERCENT_STEP);
                percentage = percentage - 0.5 * percentage; //userValue.ComputationPercentage;

                foreach (var item in values)
                    if (!dictionary2.ContainsKey(item.Key))
                        if (item.Key != EnumCategories.SoiaLover)
                            dictionary2.Add(item.Key, item.Value * userValue.PercentageSet);
                        //dictionary2.Add(item.Key, item.Value * percentage);
                        else
                            PercentageSoiaLover += userValue.PercentageSet /*percentage*/* item.Value;
                    else
                        dictionary2[item.Key] += userValue.PercentageSet /*percentage*/* item.Value;
            }
        }

        private void ComputeAbsoluteTotalPercentage()
        {
            foreach (var userValue in UserValues)
            {
                var values = userValue.dictionaryAbsoluteValues;
                var percentage = userValue.ComputationPercentage;

                foreach (var item in values)
                    if (!dictionaryAbsoluteValues.ContainsKey(item.Key))
                        dictionaryAbsoluteValues.Add(item.Key, item.Value * percentage);
                    else
                        dictionaryAbsoluteValues[item.Key] += percentage * item.Value;
            }
        }

        private void MergePorcAndPui()
        {
            PuiAndPorcPercentages = PuiAndPorcPercentages.Union(FavoritePorcPercentages)
                .ToDictionary(i => i.Key, i => i.Value);
            foreach (var kvp in FavoritePuiPercentages)
            {
                double value;
                if (!PuiAndPorcPercentages.TryGetValue(kvp.Key, out value))
                    PuiAndPorcPercentages.Add(kvp.Key, kvp.Value);
                else
                    PuiAndPorcPercentages[kvp.Key] = kvp.Value;
            }

            PuiAndPorcPercentages =
                PuiAndPorcPercentages.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        }


        private void MergePorcAndPuiAndVita()
        {
            PuiAndPorcAndVitaPercentages = PuiAndPorcAndVitaPercentages.Union(FavoritePorcPercentages)
                .ToDictionary(i => i.Key, i => i.Value);
            foreach (var kvp in FavoritePuiPercentages)
            {
                double value;
                if (!PuiAndPorcAndVitaPercentages.TryGetValue(kvp.Key, out value))
                    PuiAndPorcAndVitaPercentages.Add(kvp.Key, kvp.Value);
                else
                    PuiAndPorcAndVitaPercentages[kvp.Key] = kvp.Value;
            }

            foreach (var kvp in FavoriteVitaPercentages)
            {
                double value;
                if (!PuiAndPorcAndVitaPercentages.TryGetValue(kvp.Key, out value))
                    PuiAndPorcAndVitaPercentages.Add(kvp.Key, kvp.Value);
                else
                    PuiAndPorcAndVitaPercentages[kvp.Key] = kvp.Value;
            }

            PuiAndPorcAndVitaPercentages =
                PuiAndPorcAndVitaPercentages.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        }

        private void ComputeFavoriteChoicesPercentages()
        {
            foreach (var item in FavoriteChoises)
                item.Percentage = (double) item.SelectedCount / item.Apparences;

            FavoriteChoisesForType1 = FavoriteChoises.Where(x => x.Type == "1").ToList();
            FavoriteChoisesForType2 = FavoriteChoises.Where(x => x.Type == "2").ToList();

            if (FavoriteChoisesForType1.Count > 0)
            {
                var maxAppearancesForType1 = (int) FavoriteChoisesForType1.Average(x => x.Apparences);
                //- ConstantValues.MARGIN;
                //FavoriteChoises = FavoriteChoises.OrderByDescending(x=>x.Percentage).ToList();

                //foreach (var item in FavoriteChoisesForType1)
                //{
                //    if (item.Apparences < maxAppearancesForType1 / 2)
                //    {
                //        item.Percentage = item.Percentage * ((double) item.Apparences / maxAppearancesForType1);
                //    }
                //}

                HatedChoisesForType1 = FavoriteChoisesForType1.Where(
                        x =>
                            //x.Apparences > ConstantValues.THRESHOLD &&
                                x.Percentage < ConstantValues.HATED_THRESHOLD_1)
                    .OrderByDescending(x => x.Percentage).ThenByDescending(x => x.Apparences)
                    .ToList();

                FavoriteChoisesForType1 =
                    FavoriteChoisesForType1.Where(
                            x =>
                                x.Apparences > ConstantValues.THRESHOLD &&
                                x.Percentage > ConstantValues.PERCENTAGE_MARGIN)
                        .OrderByDescending(x => x.Percentage).ThenByDescending(x => x.Apparences)
                        .ToList();
            }

            if (FavoriteChoisesForType2.Count > 0)
            {
                var maxAppearancesForType2 = (int) FavoriteChoisesForType2.Average(x => x.Apparences);
                //- ConstantValues.MARGIN;

                //foreach (var item in FavoriteChoisesForType2)
                //{
                //    if (item.Apparences < maxAppearancesForType2 / 2)
                //    {
                //        item.Percentage = item.Percentage * ((double) item.Apparences / maxAppearancesForType2);
                //    }
                //}
                //FavoriteChoises = FavoriteChoises.OrderByDescending(x=>x.Percentage).ToList();

                HatedChoisesForType2 = FavoriteChoisesForType2.Where(
                        x =>
                            //x.Apparences > ConstantValues.THRESHOLD &&
                                x.Percentage < ConstantValues.HATED_THRESHOLD_2)
                    .OrderByDescending(x => x.Percentage).ThenByDescending(x => x.Apparences)
                    .ToList();

                FavoriteChoisesForType2 =
                    FavoriteChoisesForType2.Where(
                            x =>
                                x.Apparences > ConstantValues.THRESHOLD &&
                                x.Percentage > ConstantValues.PERCENTAGE_MARGIN)
                        .OrderByDescending(x => x.Percentage).ThenByDescending(x => x.Apparences)
                        .ToList();
            }
        }

        private void CreatePorcPercentageDictionary()
        {
            foreach (var userItem in FavoritePorcIngredients)
            {
                var categoryItem = GlobalVariables.PorkRecipes[userItem.Key];
                var percentage = (double) userItem.Value / categoryItem;

                FavoritePorcPercentages.Add(userItem.Key, percentage);
            }

            FavoritePorcPercentages =
                FavoritePorcPercentages.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        }

        private void CreatePuiPercentageDictionary()
        {
            foreach (var userItem in FavoritePuiIngredients)
            {
                var categoryItem = GlobalVariables.PuiRecipes[userItem.Key];
                var percentage = (double) userItem.Value / categoryItem;

                FavoritePuiPercentages.Add(userItem.Key, percentage);
            }

            FavoritePuiPercentages =
                FavoritePuiPercentages.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        }

        private void CreateVitaPercentageDictionary()
        {
            foreach (var userItem in FavoriteVitaIngredients)
            {
                var categoryItem = GlobalVariables.VitaRecipes[userItem.Key];
                var percentage = (double) userItem.Value / categoryItem;

                FavoriteVitaPercentages.Add(userItem.Key, percentage);
            }

            FavoriteVitaPercentages =
                FavoriteVitaPercentages.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        }
    }
}