﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodechefProject.Models.User
{
    public class UserMenu
    {
        public string Day { get; set; }
        public string Date { get; set; }
        public string F1 { get; set; }
        public string F2 { get; set; }
    }
}
