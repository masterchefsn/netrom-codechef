﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodechefProject.Models.Receipe
{
    public class Receipe
    {
        public string Name { get; set; }
        public List<string> Ingredients { get; set; }
    }
}
