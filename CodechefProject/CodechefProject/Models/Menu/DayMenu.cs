﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodechefProject.Models.Menu
{
    public class DayMenu
    {
        public string Date { get; set; }
        public List<MenuItem> MenuItems { get; set; }
    }
}
