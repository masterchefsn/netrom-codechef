﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodechefProject.Models.Menu
{
    public class WeekMenu
    {
        public int ID { get; set; }
        public string Monday { get; set; }
        public List<DayMenu> DayMenus { get; set; }
    }
}
