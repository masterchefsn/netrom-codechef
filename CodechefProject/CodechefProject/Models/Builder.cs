﻿using System.Collections.Generic;
using System.Linq;
using CodechefProject.Constants;
using CodechefProject.Helpers;

namespace CodechefProject.Models
{
    public class Builder
    {
        protected void ComputeDictionaryFromRecipes(Dictionary<string, int> dictionary, string menuDescription)
        {
            var description = menuDescription.ToLower();
            var receipesWithoutSalad = GlobalVariables.Receipes.Where(r => !Filters.IsSalataStandAlone(r.Name)
                                                                           && !Filters.HasAdditionalSalad(r.Name));
            foreach (var rec in receipesWithoutSalad)
            {
                var recipe = rec.Name.ToLower();

                if (description.Contains(recipe))
                    if (dictionary.ContainsKey(recipe))
                        dictionary[recipe]++;
                    else
                        dictionary.Add(recipe, 1);
            }
        }
    }
}