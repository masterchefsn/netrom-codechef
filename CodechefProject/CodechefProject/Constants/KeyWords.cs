﻿using System.Collections.Generic;
using CodechefProject.Models.User;

namespace CodechefProject.Constants
{
    public static class KeyWords
    {
        public static List<string> postList = new List<string> {"fara carne", "post"};

        public static List<string> CiorbeList = new List<string>
            {"ciorba", "bors", "dresala", "minestrone", "supa"};

        public static List<string> CremeList = new List<string> {"crema"};
        public static List<string> FructeList = new List<string> {"fruct"};

        public static List<string> PrajituriList = new List<string>
        {
            "amandina",
            "vis de",
            "savarina",
            "carpati",
            "diplomat",
            "petre",
            "ecler",
            "visine",
            "lady",
            "michael",
            "tarta",
            "sisi",
            "choix",
            "toffi",
            "prajitura",
            "cremsnit",
            "mozart",
            "amarena",
            "pasiuni",
            "ambasador",
            "boema",
            "buburuza",
            "bercolade",
            "salam de biscuiti",
            "felie de ciocolata",
            "joffre",
            "delicatesa",
            "croissant cu caise",
            "croissant cu ciocolata"
        };

        public static List<string> PasteList = new List<string>
        {
            "penne",
            "bolognese",
            "paste carbonara",
            "paste al forno",
            "macaroane",
            "spaghete",
            "tagliatelle",
            "paste milaneze",
            "paste amatriciana"
        };


        public static List<string> PuiList = new List<string>
        {
            "pui",
            "cotoi",
            "arip",
            "gaina",
            "curcan",
            "pipota",
            "pipote",
            "inimi",
            "pulpe inferioare",
            "pulpe dezosate",
            "pulpa dez",
            "pulpe la cuptor"
        };

        public static List<string> PorcList = new List<string>
        {
            "porc",
            "cotlet",
            "mici",
            "scarite",
            "tigaie",
            "afumatura",
            "ciolan",
            "maruntaie",
            "burta",
            "perisoare",
            "chiftele marinate",
            "parjoale",
            "friptura",
            "steak",
            "frigarui",
            "carnaciori",
            "carnati",
            "kaizer",
            "ceafa",
            "chiftelute marinate",
            "ficatei",
            "sarmale",
            "sarmalute",
            "svabesc",
            "toscan",
            "ardei umplut",
            "musaca",
            "a la cluj",
            "carne",
            "cordon bleu",
        };

        public static List<string> VitaList = new List<string> { "vita", "vacuta", "vaca", "vitel"};//, "miel"};
        public static List<string> carnar = new List<string> {"carnar"};

        public static List<string> PesteList = new List<string>
        {
            "plachie",
            "peste",
            "macrou",
            "crap",
            "pastrav",
            "somon",
            "anchoa",
            "hering",
            "pescaresc",
            "sprot"
        };

        public static readonly List<string> Apostol = new List<string> {"Apostol"};
        public static readonly List<string> Regim = new List<string> {"Regim"};
        public static readonly List<string> Nono1 = new List<string> {"Nono1"};
        public static readonly List<string> Nono2 = new List<string> {"Nono2"};
        public static readonly List<string> soia = new List<string> {"soia"};
        public static readonly List<string> salata = new List<string> {"salata"};

        public static readonly List<string> PartyMeal = new List<string>
        {
            "meniu de paste",
            "brunch",
            "meniu surpriza",
            "pranz traditional craciun"
        };

        public static List<string> GetKeyWords(EnumCategories category)
        {
            switch (category)
            {
                case EnumCategories.CiorbarCarnivor:
                    return CiorbeList;
                case EnumCategories.CiorbarErbivor:
                    return CiorbeList;
                case EnumCategories.Supar:
                    return CremeList;
                case EnumCategories.Prajiturar:
                    return PrajituriList;
                case EnumCategories.Fructar:
                    return FructeList;
                case EnumCategories.NoNo1:
                    return Nono1;

                case EnumCategories.Porcar:
                    return PorcList;
                case EnumCategories.Apostol:
                    return Apostol;
                case EnumCategories.Puiar:
                    return PuiList;
                case EnumCategories.Pescar:
                    return PesteList;
                case EnumCategories.Macaronar:
                    return PasteList;
                case EnumCategories.NoNo2:
                    return Nono2;
                case EnumCategories.SoiaLover:
                    return soia;
                case EnumCategories.Salatar:
                    return salata;
                case EnumCategories.Vitar:
                    return VitaList;
                case EnumCategories.Carnar:
                    return carnar;

                case EnumCategories.RegimarOnMonday:
                case EnumCategories.RegimarOnTuesday:
                case EnumCategories.RegimarOnWednesday:
                case EnumCategories.RegimarOnThursday:
                case EnumCategories.RegimarOnFriday:
                    return Regim;
                //case EnumCategories.Salatar
                //return sal
                default:
                    return PorcList;
            }
        }
    }
}