﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodechefProject.Models.Menu;
using CodechefProject.Models.Receipe;

namespace CodechefProject.Constants
{
    public class GlobalVariables
    {
        public static Dictionary<string, int> PorkRecipes = new Dictionary<string, int>();
        public static Dictionary<string, int> VitaRecipes = new Dictionary<string, int>();
        public static Dictionary<string, int> PuiRecipes = new Dictionary<string, int>();
        public static List<WeekMenu> WeekMenus = new List<WeekMenu>();
        public static List<Receipe> Receipes = new List<Receipe>();
    }
}
