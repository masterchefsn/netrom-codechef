﻿using System;
using System.Collections.Generic;

namespace CodechefProject.Constants
{
    public class ConstantValues
    {
        public static int MIE = 1000;
        public static string baseapi = "http://codechefapi.stage02.netromsoftware.ro/api/YfE6qGCKNHpAaSIXJrzr";

        public static double PuiPorcAllowedDifference = 0.1;
        public static DateTime StartDate = new DateTime(2016, 10, 3);

        //the api available start date
        //public static DateTime InfoStartDate = new DateTime(2016, 2, 1);
        public static DateTime EndDate = new DateTime(2017, 4, 14);
        
        //public static DateTime EndDate = new DateTime(2017, 2, 24);
        public static double PERCENTAGE_MARGIN = 0.40;
        public static double THRESHOLD = 2;

        public static double HATED_THRESHOLD_1 = 0.15;
        public static double HATED_THRESHOLD_2 = 0.15;
        public static int DAYS_STEP = -30;
        public static List<string> DayNames = new List<string> {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};

        //public static int MARGIN = 10;
        public static double PERCENT_STEP = 0.20d;
        public static double HATED_INGREDIENT_PERCENT = 0.1d;

    }
}