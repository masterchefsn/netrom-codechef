﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using CodechefProject.CategoryManagement;
using CodechefProject.Constants;
using CodechefProject.Helpers;
using CodechefProject.Models;
using CodechefProject.Models.Menu;
using CodechefProject.Models.User;
using CodechefProject.Service;

namespace CodechefProject.UserManagement
{
    public class UsersProfileBuilder : Builder
    {
        private readonly CategoryBuilder _categoryBuilder = new CategoryBuilder();
        private readonly MenuService _menuService;


        public UsersProfileBuilder()
        {
            _menuService = new MenuService();
        }

        public void CreateUsersProfile(List<User> users, List<WeekMenu> weekMenus)
        {
            var dayMenus = weekMenus.SelectMany(x => x.DayMenus.SelectMany(d => d.MenuItems));

            foreach (var menu in dayMenus)
            {
                menu.Description = menu.Description.Split('.').LastOrDefault();

                if (menu.Description.Length > 0)
                    menu.Description = menu.Description.Substring(1);
            }

            for (var i = 0; i < users.Count; i++)
            {
                var user = users[i];

                foreach (var item in dayMenus)
                foreach (var recipe in GlobalVariables.Receipes)
                    if (item.Description.Contains(recipe.Name))
                        foreach (var ingredient in recipe.Ingredients)
                        {
                            var existingIngredient = user.UserIngredients.FirstOrDefault(
                                xy => xy.IngredientDescription == ingredient);

                            if (existingIngredient == null)
                            {
                                var newUserIngredient = new UserFavoriteIngredient();
                                newUserIngredient.IngredientDescription = ingredient;
                                user.UserIngredients.Add(newUserIngredient);
                            }
                            else
                            {
                                existingIngredient.Apparences++;
                            }
                        }

                var userMenus =
                    _menuService.GetAllUserMenusByStartEndDate(ConstantValues.StartDate.ToString("yyyy-MM-dd"),
                        ConstantValues.EndDate.ToString("yyyy-MM-dd"), user.UID);

                userMenus.RemoveMenusAfterSixOclock();

                user.ComputeTotalNumberOfMenus(userMenus);
                //user.MenusOnDays = new MenusDaysOfWeek(userMenus);

                Util.ChangedDateFormat(userMenus);

                if (userMenus.Count < 1)
                    continue;

                var weekMenu = weekMenus.GetAllWeekMenuItemsByDateOfWeek(userMenus[0].Day);
                if (weekMenu == null)
                    continue;

                weekMenu = weekMenu.ChangedDateFormat();
                userMenus = Util.ChangedDateFormat(userMenus);

                foreach (var userMenu in userMenus)
                {
                    if (weekMenu == null ||
                        Convert.ToDateTime(userMenu.Day) > Convert.ToDateTime(weekMenu.DayMenus.Last().Date))
                        weekMenu = weekMenus.GetAllWeekMenuItemsByDateOfWeek(userMenu.Day);

                    if (weekMenu == null) continue;

                    var dayMenu = weekMenu.DayMenus.FirstOrDefault(x => x.Date == userMenu.Day);

                    if (dayMenu == null)
                        continue;

                    var xx = GlobalVariables.PorkRecipes;
                    var menuItems = dayMenu.MenuItems;

                    var selectedMenuItem1 = menuItems.FirstOrDefault(x => x.Code == userMenu.F1);
                    var selectedMenuItem2 = menuItems.FirstOrDefault(x => x.Code == userMenu.F2);

                    var favChoise = dayMenus.Where(x => x == selectedMenuItem1 || x == selectedMenuItem2);

                    if (selectedMenuItem1?.Description == "Fructe (1 bucata)" ||
                        selectedMenuItem1?.Description == "Fructe")
                        selectedMenuItem1.Description = "Fructe - 1 bucata";

                    foreach (var recipe in GlobalVariables.Receipes)
                    {
                        if (selectedMenuItem1 != null)
                            if (selectedMenuItem1.Description.Contains(recipe.Name))
                                foreach (var ri in recipe.Ingredients)
                                {
                                    var existingIngredient = user.UserIngredients.FirstOrDefault
                                        (x => x.IngredientDescription == ri);

                                    if (existingIngredient != null)
                                        existingIngredient.SelectedCount++;
                                }

                        if (selectedMenuItem2 != null)
                            if (selectedMenuItem2.Description.Contains(recipe.Name))
                                foreach (var ri in recipe.Ingredients)
                                {
                                    var existingIngredient = user.UserIngredients.FirstOrDefault
                                        (x => x.IngredientDescription == ri);

                                    if (existingIngredient != null)
                                        existingIngredient.SelectedCount++;
                                }
                    }


                    var existingFavChoise
                        = user.FavoriteChoises.Where(x => selectedMenuItem1 != null &&
                                                          (x.FoodDescription == selectedMenuItem1.Description ||
                                                           selectedMenuItem1.Description.Contains(x.FoodDescription))
                                                          || selectedMenuItem2 != null
                                                          &&
                                                          (x.FoodDescription ==
                                                           selectedMenuItem2.Description
                                                           ||
                                                           selectedMenuItem2.Description.Contains(
                                                               x.FoodDescription)
                                                          ));

                    if (existingFavChoise.Any())
                        foreach (var existingCh in existingFavChoise)
                        {
                            var foodTotalApparences = dayMenus.Count(x =>
                                x.Description == existingCh.FoodDescription ||
                                x.Description.Contains(existingCh.FoodDescription));
                            user.FavoriteChoises[user.FavoriteChoises.IndexOf(existingCh)].Apparences =
                                foodTotalApparences;
                            user.FavoriteChoises[user.FavoriteChoises.IndexOf(existingCh)].SelectedCount++;
                            user.FavoriteChoises[user.FavoriteChoises.IndexOf(existingCh)].Type = existingCh.Type;
                        }
                    if (favChoise.Any() && existingFavChoise.Count() == 0)
                        foreach (var ch in favChoise)
                        {
                            if (ch.Description == string.Empty)
                                continue;

                            var favCh = new UserFavoriteChoises
                            {
                                FoodDescription = ch.Description.TrimEnd()
                            };
                            favCh.Type = ch.Type;
                            favCh.SelectedCount++;
                            var foodTotalApparences =
                                dayMenus.Count(x => x.Description == ch.Description.Split('.').LastOrDefault());
                            favCh.Apparences = foodTotalApparences;
                            user.FavoriteChoises.Add(favCh);
                        }

                    if (selectedMenuItem2 != null)
                        CheckExtraMenu(selectedMenuItem2,
                            menuItems.Where(x => x.Type == "2" && x.Code != selectedMenuItem2.Code).ToList(), user);
                    var procent = 1; //Util.GetPercent(userMenu.Day);

                    var userValues = user.GetUserValues(Convert.ToDateTime(userMenu.Day));

                    CheckMenuItem1(selectedMenuItem1, user, procent, userValues);
                    CheckMenuItem2(selectedMenuItem2, user, dayMenu, procent, userValues);
                }

                Thread.Sleep(200);
            }
        }

        private void CheckMenuItem1(MenuItem selectedMenuItem1, User user, double procent, UserValues userValues)
        {
            if (selectedMenuItem1 != null)
            {
                var selected = false;
                if (Filters.IsCrema(selectedMenuItem1.Description))
                {
                    userValues.Cremos = userValues.Cremos + procent;
                    selected = true;
                }

                if (Filters.IsCiorba(selectedMenuItem1.Description))
                    if (Filters.ContainsMeat(selectedMenuItem1.Description) &&
                        !Filters.IsPostMeal(selectedMenuItem1.Description))
                    {
                        if (!selected)
                            userValues.CiorbarCarnivor = userValues.CiorbarCarnivor + procent;
                        selected = true;
                    }
                    else
                    {
                        if (!selected)
                            userValues.CiorbarErbivor = userValues.CiorbarErbivor + procent;
                        selected = true;
                    }

                if (Filters.IsPrajitura(selectedMenuItem1.Description))
                {
                    if (!selected)
                        userValues.Prajiturar = userValues.Prajiturar + procent;
                    selected = true;
                }

                if (Filters.IsFruct(selectedMenuItem1.Description))
                {
                    if (!selected)
                        userValues.Fructar = userValues.Fructar + procent;
                    selected = true;
                }

                if (Filters.IsPartyMeal(selectedMenuItem1.Description))
                    return;

                if (!selected)
                    using (var sw = File.AppendText(@"d:\7000.txt"))
                    {
                        sw.WriteLine("Fel 1: " + selectedMenuItem1.Description.ToLower());
                    }
            }
            else
            {
                userValues.NoNo1 = userValues.NoNo1 + procent;
            }
        }


        private void CheckMenuItem2(MenuItem selectedMenuItemType2, User user, DayMenu dayMenu,
            double procent, UserValues userValues)
        {
            var dayMenuDate = Convert.ToDateTime(dayMenu.Date);

            // var semiPostDay = Filters.IsSemiPostDay(dt);
            userValues.MenusOnDays.UpdateTotalNumber(dayMenu.Date);

            if (selectedMenuItemType2 != null)
            {
                var selected = false;

                if (Filters.IsPostMeal(selectedMenuItemType2.Description))
                {
                    userValues.Apostol = userValues.Apostol + procent;
                    selected = true;
                }

                if (Filters.NoMeat(selectedMenuItemType2.Description.ToLower()))
                {
                    var day = dayMenuDate.DayOfWeek;
                    userValues.RegimNumberOnDay[day] = userValues.RegimNumberOnDay[day] + procent;
                    selected = true;
                }

                /** todo -> ingrediente salata -> pentru a sti daca e de post sau nu*/

                if (Filters.IsSalataStandAlone(selectedMenuItemType2.Description))
                {
                    userValues.Salatar = userValues.Salatar + procent;
                    return;
                }

                if (Filters.IsPuiMeal(selectedMenuItemType2.Description))
                    if (!selected)
                    {
                        //if (!selectedMenuItemType2.Description.Contains("si salata"))
                        ComputeDictionaryFromRecipes(user.FavoritePuiIngredients, selectedMenuItemType2.Description);
                        userValues.Puiar = userValues.Puiar + procent;
                        return;
                    }

                if (Filters.IsVitaMeal(selectedMenuItemType2.Description))
                    if (!selected)
                    {
                        ComputeDictionaryFromRecipes(user.FavoriteVitaIngredients, selectedMenuItemType2.Description);
                        userValues.Vitar = userValues.Vitar + procent;
                        return;
                    }

                if (Filters.IsSoiaMeal(selectedMenuItemType2.Description))
                {
                    userValues.SoiaLover = userValues.SoiaLover + procent;
                    return;
                }

                if (Filters.IsPorcMeal(selectedMenuItemType2.Description))
                    if (!selected)
                    {
                        ComputeDictionaryFromRecipes(user.FavoritePorcIngredients, selectedMenuItemType2.Description);
                        userValues.Porcar = userValues.Porcar + procent;
                        return;
                    }

                if (Filters.IsPesteMeal(selectedMenuItemType2.Description))
                {
                    userValues.Pescar = userValues.Pescar + procent;
                    return;
                }


                if (Filters.IsPasteMeal(selectedMenuItemType2.Description))
                {
                    userValues.Macaronar = userValues.Macaronar + procent;
                    return;
                }

                if (!selected)
                    if (selectedMenuItemType2.Description.ToLower() != "-")
                    {
                        userValues.Apostol = userValues.Apostol + procent;
                        using (var sw = File.AppendText(@"d:\fel2.txt"))
                        {
                            sw.WriteLine(selectedMenuItemType2.Description.ToLower());
                        }
                    }
            }
            else
            {
                userValues.NoNo2 = userValues.NoNo2 + procent;
            }
        }

        /**
         ** Verifica utilizatorii ce nu isi doresc si salata la felul de mancare
         */

        private void CheckExtraMenu(MenuItem selectedMenuItem, List<MenuItem> items, User user)
        {
            var selectedDescription = !string.IsNullOrEmpty(selectedMenuItem.Description)
                ? selectedMenuItem.Description.RemoveCodeFromDescription()
                : string.Empty;
            var descriptionListString = items.Select(x => x.Description).ToList().RemoveCodeFromDescriptionInList();

            foreach (var item in descriptionListString)
            {
                if (item.Contains(selectedDescription))
                {
                    user.ExtraLover--;
                    break;
                }

                if (selectedDescription.Contains(item))
                {
                    user.ExtraLover++;
                    break;
                }
            }
        }
    }
}