﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CodechefProject.Constants;
using CodechefProject.Helpers;
using CodechefProject.Models.Menu;
using CodechefProject.Models.User;
using CodechefProject.Service;

namespace CodechefProject.UserManagement
{
    public class UserChoiceSelector
    {
        private readonly MenuService _menuService = new MenuService();
        private readonly UsersService userService = new UsersService();

        public void SelectChoice(List<User> users)
        {
            var endDate = new DateTime(2017, 5, 5);

            for (var i = 0; i < users.Count; i++)
            {
                var user = users[i];

                //user.Compute(monthCategoryContainer);

                var result = new List<UserMenu>();
                var currentDate = new DateTime(2017, 4, 17);

                while (currentDate < endDate)
                {
                    var currentWeekMenu =
                        GlobalVariables.WeekMenus.GetAllWeekMenuItemsByDateOfWeek(currentDate.ToString("yyyy-MM-dd"));

                    if (currentWeekMenu == null)
                    {
                        currentWeekMenu =
                            _menuService.GetAllWeekMenuItemsByDateOfWeek(currentDate.ToString("yyyy-MM-dd"));
                        currentWeekMenu = currentWeekMenu.ChangedDateFormat();

                        var dayMenus = currentWeekMenu.DayMenus.SelectMany(d => d.MenuItems);

                        foreach (var menu in dayMenus)
                        {
                            menu.Description = menu.Description.Split('.').LastOrDefault();

                            if (menu.Description.Length > 0)
                                menu.Description = menu.Description.Substring(1);
                        }

                        GlobalVariables.WeekMenus.Add(currentWeekMenu);
                    }

                    currentDate = currentDate.AddDays(7);

                    //if (user.Name == "Bertha Carter")
                    //{
                    //    this.WriteMenus(currentWeekMenu.DayMenus);
                    //}

                    foreach (var dayMenu in currentWeekMenu.DayMenus)
                    {
                        var userMenu = new UserMenu
                        {
                            Date = DateTime.Now.ToString("yyyy-MM-dd"),
                            Day = dayMenu.Date
                        };

                        if (dayMenu.MenuItems.Where(x => x.Type == "1" && x.Code == "-").ToList().Count == 1
                            && dayMenu.MenuItems.Where(x => x.Type == "2" && x.Code == "-").ToList().Count == 1
                        )
                            continue;

                        SetType1Meal(user, userMenu, dayMenu);
                        SetType2Meal(user, userMenu, dayMenu);

                        //if (user.Name == "Bertha Carther")
                        //{
                        //    StringBuilder sb = new StringBuilder();
                        //    sb.Append(userMenu.Day);
                        //    sb.Append(" ");
                        //    //sb.Append(userMenu.)

                        //    Util.LogInFile("D://nextweek.txt", s);
                        //}

                        result.Add(userMenu);
                    }
                }

                userService.UpdateUser(user.UID, result).Wait();
            }
        }

        private void WriteMenus(List<DayMenu> dayMenus)
        {
            foreach (var dayMenu in dayMenus)
            {
                Util.LogInFile("d://test.txt", dayMenu.Date);
                foreach (var item in dayMenu.MenuItems)
                    Util.LogInFile("d://test.txt", item.Type + " " + item.Code + " " + item.Description);
            }
        }


        private void SetType2Meal(User user, UserMenu userMenu, DayMenu dayMenu)
        {
            var menu2Items = dayMenu.MenuItems.Where(x => x.Type == "2").ToList();
            var fel2Found = false;

            var dayOfWeek = Convert.ToDateTime(userMenu.Day).DayOfWeek;
            var day = Convert.ToDateTime(userMenu.Day);
            var erbivorToday = false;

            foreach (var favMenu in user.FavoriteChoisesForType2)
            {
                var menuItem =
                    menu2Items.Where(x => x.Description.TrimEnd() == favMenu.FoodDescription).ToList().FirstOrDefault();
                if (menuItem == null)
                    continue;

                if (!Filters.CheckMeatForRegimersType2(user.dictionary2, menuItem.Description, day))
                    continue;

                if (!Filters.CheckMeatForApostols(user.dictionary2, menuItem.Description))
                    continue;

                userMenu.F2 = menuItem.Code;
                fel2Found = true;
                break;
            }

            if (fel2Found)
                return;

            foreach (var item in user.dictionary2)
            {
                if (fel2Found)
                    break;

                //if (item.Value < 0.1)
                //{
                //    RandomizeType2(userMenu, dayMenu);
                //    fel2Found = true;
                //    break;
                //}

                var keyWords = KeyWords.GetKeyWords(item.Key);

                if (keyWords.Any(s => s.Contains("Nono2")))
                {
                    fel2Found = true;
                    break;
                }

                if (menu2Items.All(x => IsHatedMeal(user, x.Description, 2)))
                {
                    userMenu.F2 = "B04";
                    fel2Found = true;
                    break;
                }

                if (keyWords.Any(s => s.Contains("carnar")) && !erbivorToday)
                {
                    var filteredMenuItems =
                        menu2Items.Where(x => Filters.IsPorcMeal(x.Description) || Filters.IsPuiMeal(x.Description)
                                              || Filters.IsVitaMeal(x.Description))
                            .ToList();

                    if (filteredMenuItems.All(x => IsHatedMeal(user, x.Description, 2)))
                    {
                        continue;
                    }

                    userMenu.F2 = ChooseUserMenuForType2(user,
                        filteredMenuItems.Where(menu => !IsHatedMeal(user, menu.Description, 2)).ToList(),
                        EnumCategories.Carnar);
                    fel2Found = true;
                    break;
                }


                if (keyWords.Any(s => s.Contains("Apostol")) ||
                    keyWords.Any(k => k.Contains("Regim")) &&
                    //(dayOfWeek == DayOfWeek.Wednesday || dayOfWeek == DayOfWeek.Friday)
                    (dayOfWeek == DayOfWeek.Monday && item.Key == EnumCategories.RegimarOnMonday ||
                     dayOfWeek == DayOfWeek.Tuesday && item.Key == EnumCategories.RegimarOnTuesday ||
                     dayOfWeek == DayOfWeek.Wednesday && item.Key == EnumCategories.RegimarOnWednesday ||
                     dayOfWeek == DayOfWeek.Thursday && item.Key == EnumCategories.RegimarOnThursday ||
                     dayOfWeek == DayOfWeek.Friday && item.Key == EnumCategories.RegimarOnFriday))
                {
                    erbivorToday = true;

                    var value = GetCodeForPost(user,
                        menu2Items.Where(menu => !IsHatedMeal(user, menu.Description, 2)).ToList(), item.Key);

                    if (value == null)
                        continue;

                    userMenu.F2 = value;
                    fel2Found = true;
                    continue;
                }

                var matchList = new List<MenuItem>();
                foreach (var menuItem in menu2Items)
                    if (keyWords.Contains("salata"))
                    {
                        if (Filters.IsSalataStandAlone(menuItem.Description))
                            if (!IsHatedMeal(user, menuItem.Description, 2))
                            {
                                matchList.Add(menuItem);
                                fel2Found = true;
                            }
                    }
                    else
                    {
                        if (keyWords.Any(key => menuItem.Description.ToLower().Contains(key)))
                            if (!Filters.IsSalataStandAlone(menuItem.Description)
                                && !Filters.IsPostMeal(menuItem.Description)
                            )

                                if (erbivorToday &&
                                    (item.Key == EnumCategories.Puiar || item.Key == EnumCategories.Porcar
                                     || item.Key == EnumCategories.Carnar || item.Key == EnumCategories.Vitar))
                                {
                                    break;
                                }
                                else
                                {
                                    if (!IsHatedMeal(user, menuItem.Description, 2))
                                    {
                                        matchList.Add(menuItem);
                                        fel2Found = true;
                                    }
                                }
                    }

                if (fel2Found)
                    if (matchList.Count == 1)
                        userMenu.F2 = matchList[0].Code;
                    else
                        userMenu.F2 = ChooseUserMenuForType2(user,
                            matchList.Where(mitem => !IsHatedMeal(user, mitem.Description, 2)).ToList(), item.Key);
            }

            if (!fel2Found)
            {
                using (var sw = File.AppendText(@"d:\7001.txt"))
                {
                    sw.WriteLine(dayMenu.MenuItems.Where(x => x.Type == "2").ToList());
                }
                //Random rnd = new Random();
                //var itemsCount = menu2Items.Count();
                //var index = rnd.Next(1, itemsCount) - 1;

                //var selectedMenuItem = menu2Items[index];

                //for (var j = 0; j < menu2Items.Count; j++)
                //{
                //    if (index != j && menu2Items[j].Description.Contains(selectedMenuItem.Description))
                //    {
                //        selectedMenuItem = menu2Items[j];
                //        break;
                //    }
                //}

                //userMenu.F2 = selectedMenuItem.Code;
                userMenu.F2 = "B04";
            }
        }

        private void SetType1Meal(User user, UserMenu userMenu, DayMenu dayMenu)
        {
            var menu1Items = dayMenu.MenuItems.Where(x => x.Type == "1").ToList();
            var fel1Found = false;
            var day = Convert.ToDateTime(userMenu.Day);
            var dayOfWeek = Convert.ToDateTime(userMenu.Day).DayOfWeek;


            var erbivorToday = false;

            foreach (var favMenu in user.FavoriteChoisesForType1)
            {
                var menuItem =
                    menu1Items.Where(x => x.Description.TrimEnd() == favMenu.FoodDescription).ToList().FirstOrDefault();
                if (menuItem == null)
                    continue;

                if (!Filters.CheckMeatForRegimersType1(user.dictionary1, menuItem.Description, day))
                    continue;

                if (Filters.ContainsMeat(menuItem.Description)
                    && user.dictionary1[EnumCategories.CiorbarCarnivor] < user.dictionary2[EnumCategories.Apostol])
                    continue;

                userMenu.F1 = menuItem.Code;
                fel1Found = true;
                break;
            }


            if (fel1Found)
                return;

            foreach (var item in user.dictionary1)
            {
                if (fel1Found)
                    break;

                //if (item.Value < 0.1)
                //{
                //    RandomizeType1(userMenu, dayMenu);
                //    fel1Found = true;
                //    break;
                //}

                var keyWords = KeyWords.GetKeyWords(item.Key);

                if (keyWords.Any(s => s.Contains("Nono1")))
                {
                    fel1Found = true;
                    break;
                }

                if (keyWords.Any(s => s.Contains("Apostol")) ||
                    keyWords.Any(k => k.Contains("Regim")) &&
                    (
                        dayOfWeek == DayOfWeek.Monday && item.Key == EnumCategories.RegimarOnMonday ||
                        dayOfWeek == DayOfWeek.Tuesday && item.Key == EnumCategories.RegimarOnTuesday ||
                        dayOfWeek == DayOfWeek.Wednesday && item.Key == EnumCategories.RegimarOnWednesday ||
                        dayOfWeek == DayOfWeek.Thursday && item.Key == EnumCategories.RegimarOnThursday ||
                        dayOfWeek == DayOfWeek.Friday && item.Key == EnumCategories.RegimarOnFriday))
                {
                    erbivorToday = true;
                    continue;
                }

                foreach (var menuItem in menu1Items)
                    if (keyWords.Any(key => menuItem.Description.ToLower().Contains(key)))
                    {
                        if (keyWords.Contains("ciorba"))
                        {
                            if (item.Key == EnumCategories.CiorbarCarnivor)
                            {
                                if (erbivorToday)
                                    break;

                                if (Filters.ContainsMeat(menuItem.Description) &&
                                    !Filters.IsPostMeal(menuItem.Description))
                                    if (!IsHatedMeal(user, menuItem.Description, 1))
                                    {
                                        userMenu.F1 = menuItem.Code;
                                        fel1Found = true;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                else
                                    continue;
                            }
                            else
                            {
                                if (!Filters.ContainsMeat(menuItem.Description) &&
                                    !Filters.IsCrema(menuItem.Description))
                                    if (!IsHatedMeal(user, menuItem.Description, 1))
                                    {
                                        userMenu.F1 = menuItem.Code;
                                        fel1Found = true;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                else
                                    continue;
                            }
                        }
                        else
                        {
                            if (keyWords.Contains("prajitura"))
                                if (menuItem.Code != "B03")
                                {
                                    var truePraji = menu1Items.FirstOrDefault(x => x.Code == "B03");

                                    if (truePraji != null &&
                                        keyWords.Any(key => truePraji.Description.ToLower().Contains(key)))
                                        if (!IsHatedMeal(user, menuItem.Description, 1))
                                        {
                                            userMenu.F1 = truePraji.Code;
                                            fel1Found = true;
                                        }
                                        else
                                        {
                                            continue;
                                        }
                                    else
                                        break;
                                }
                                else
                                {
                                    if (!IsHatedMeal(user, menuItem.Description, 1))
                                    {
                                        userMenu.F1 = menuItem.Code;
                                        fel1Found = true;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            if (!fel1Found)
                                if (!IsHatedMeal(user, menuItem.Description, 1))
                                {
                                    userMenu.F1 = menuItem.Code;
                                    fel1Found = true;
                                }
                                else
                                {
                                    continue;
                                }
                        }
                        break;
                    }
            }

            if (!fel1Found)
                userMenu.F1 = "B01";
        }

        private void RandomizeType1(UserMenu userMenu, DayMenu dayMenu)
        {
            var random = new Random();
            var count1 = dayMenu.MenuItems.Count(x => x.Type == "1");

            var type1Random = random.Next(0, count1 - 1);
            userMenu.F1 = dayMenu.MenuItems[type1Random].Code;
        }

        private void RandomizeType2(UserMenu userMenu, DayMenu dayMenu)
        {
            var random = new Random();
            var count2 = dayMenu.MenuItems.Count(x => x.Type == "2");

            var type2Random = random.Next(0, count2 - 1);
            userMenu.F2 = dayMenu.MenuItems[type2Random].Code;
        }

        private string ChooseUserMenuForType2(User user, List<MenuItem> matchList, EnumCategories category)
        {
            var filteredList = new List<MenuItem>();
            if (category == EnumCategories.Porcar)
            // || monthCategoryContainer == EnumCategories.Puiar || monthCategoryContainer == EnumCategories.Vitar)
            {
                foreach (var dictionaryItem in user.FavoritePorcPercentages)
                {
                    filteredList =
                        matchList.Where(x => x.Description.ToLower().Contains(dictionaryItem.Key)
                                             && !Filters.IsSalataStandAlone(x.Description)).ToList();

                    if (filteredList.Count > 0)
                        break;
                }

                if (filteredList.Count == 0)
                    filteredList = matchList;
            }
            else
            {
                if (category == EnumCategories.Carnar)
                {
                    foreach (var dictionaryItem in user.PuiAndPorcAndVitaPercentages)
                    {
                        filteredList =
                            matchList.Where(x => x.Description.ToLower().Contains(dictionaryItem.Key)
                                                 && !Filters.IsSalataStandAlone(x.Description)).ToList();

                        if (filteredList.Count > 0)
                            break;
                    }

                    if (filteredList.Count == 0)
                        filteredList = matchList;
                }
                else
                {
                    filteredList = matchList;
                }
            }

            MenuItem menuItem;
            if (user.ExtraLover >= 0)
            {
                menuItem =
                    filteredList.FirstOrDefault(
                        y => y.Description.Length == filteredList.Max(x => x.Description.Length));
            }

            else
            {
                if (user.ExtraLover < 0)
                    menuItem =
                        filteredList.FirstOrDefault(
                            y => y.Description.Length == filteredList.Min(x => x.Description.Length));
                else
                    menuItem = GetRandomMenu(filteredList);
            }

            return menuItem.Code;
        }


        private MenuItem GetRandomMenu(List<MenuItem> menuItems)
        {
            var count = menuItems.Count();

            var rnd = new Random();
            var index = rnd.Next(0, count);

            return menuItems[index];
        }

        private static string GetCodeForPost(User user, List<MenuItem> items, EnumCategories key)
        {
            var list = new List<MenuItem>();
            //int length = 0;
            var maxLength = 0;
            foreach (var menuItem in items)
                if (!Filters.IsSalataStandAlone(menuItem.Description) && !Filters.IsPasteMeal(menuItem.Description))

                    //&& !Filters.IsPesteMeal(menuItem.Description)
                    if (
                        Filters.IsPostMeal(menuItem.Description) && !Filters.IsSalataStandAlone(menuItem.Description) ||
                        Filters.NoMeatOrFish(menuItem.Description) && !Filters.IsSoiaMeal(menuItem.Description) ||
                        Filters.IsSoiaMeal(menuItem.Description) && user.dictionary2[key] / 5 < user.PercentageSoiaLover

                    //&& !Filters.IsSoiaMeal(menuItem.Description)
                    //&& !Filters.IsSalataStandAlone(menuItem.Description)
                    // && !Filters.IsPasteMeal(menuItem.Description)
                    )

                    {
                        list.Add(menuItem);
                        var length = menuItem.Description.Length;

                        if (length > maxLength)
                            maxLength = length;
                    }

            if (list.Count > 2)
                if (list.Any(x => Filters.IsSoiaMeal(x.Description)))
                    if (user.PercentageSoiaLover < user.dictionary2[key] / 2)
                        list.RemoveAll(x => Filters.IsSoiaMeal(x.Description));

            if (list.Count > 0)
                if (user.ExtraLover > -1)
                {
                    var biggestMenu = list.FirstOrDefault(x => x.Description.Length == maxLength);
                    return biggestMenu.Code;
                }
                else
                {
                    var samllestMenu =
                        list.FirstOrDefault(x => x.Description.Length == list.Min(y => y.Description.Length));
                    return samllestMenu.Code;
                }
            return null;
        }

        private bool IsHatedMeal(User user, string description, int type)
        {
            if (type == 1)
            {
                var hated = user.HatedChoisesForType1;
                if (hated.Any(x => description.Contains(x.FoodDescription)))
                    return true;
            }
            else
            {
                var hated = user.HatedChoisesForType2;
                if (hated.Any(x => description.Contains(x.FoodDescription)))
                    return true;
            }

            var hatedRecipies = new List<string>();
            foreach (var hatedIngredient in user.UserHatedIngredients)
            {
                var recipes = GlobalVariables.Receipes.Where
                    (x => x.Ingredients.Contains(hatedIngredient.IngredientDescription)).Select(x => x.Name);

                hatedRecipies.AddRange(recipes);
            }

            foreach (var hated in hatedRecipies)
            {
                if (description.Contains(hated))
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsFavoriteMeal(User user, string description, int type)
        {
            if (type == 1)
            {
                var favorite = user.FavoriteChoisesForType1;
                if (favorite.Any(x => description.Contains(x.FoodDescription)))
                    return true;
            }
            else
            {
                var favorite = user.FavoriteChoisesForType2;
                if (favorite.Any(x => description.Contains(x.FoodDescription)))
                    return true;
            }

            var favoriteRecipies = new List<string>();
            foreach (var favoriteIngredient in user.UserFavoriteIngredients)
            {
                var recipes = GlobalVariables.Receipes.Where
                    (x => x.Ingredients.Contains(favoriteIngredient.IngredientDescription)).Select(x => x.Name);

                favoriteRecipies.AddRange(recipes);
            }

            foreach (var favorite in favoriteRecipies)
            {
                if (description.Contains(favorite))
                {
                    return true;
                }
            }

            return false;
        }
    }
}