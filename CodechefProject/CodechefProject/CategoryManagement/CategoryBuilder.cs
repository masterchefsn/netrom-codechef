﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodechefProject.Constants;
using CodechefProject.Helpers;
using CodechefProject.Models;
using CodechefProject.Models.Menu;
using CodechefProject.Models.User;

namespace CodechefProject.CategoryManagement
{
    public class CategoryBuilder : Builder
    {
        public void ComputeTotalChoices(DayMenu dayMenu, AppCategories appCategories)
        {
            //var category = appCategories.Categories.FirstOrDefault(x => x.EndDate >= Convert.ToDateTime(dayMenu.Date)
            //                                                            &&
            //                                                            x.StartDate <= Convert.ToDateTime(dayMenu.Date));

            var category =
                appCategories.Categories.FirstOrDefault(x => x.EndDate.Month == Convert.ToDateTime(dayMenu.Date).Month
                                                             &&
                                                             x.EndDate.Year == Convert.ToDateTime(dayMenu.Date).Year);

            var percentage = 1; //1 - Util.GetPercent(dayMenu.EndDate);
            ComputeType1(dayMenu.MenuItems.Where(x => x.Type == "1").ToList(), category, percentage);
            ComputeType2(dayMenu.MenuItems.Where(x => x.Type == "2" && !x.Description.Contains("si salata")).ToList(),
                category, percentage);
        }

        private void ComputeType1(List<MenuItem> menuItems, MonthCategoryContainer monthCategoryContainer,
            double percentage)
        {
            foreach (var item in menuItems)
            {
                if (Filters.IsCrema(item.Description))
                {
                    monthCategoryContainer.NrCreme = monthCategoryContainer.NrCreme + percentage;
                    continue;
                }

                if (Filters.IsCiorba(item.Description))
                {
                    if (Filters.ContainsMeat(item.Description)
                        && !Filters.IsPostMeal(item.Description))
                        monthCategoryContainer.NrCiorbeCarne = monthCategoryContainer.NrCiorbeCarne + percentage;
                    else
                        monthCategoryContainer.NrCiorbeFaraCarne = monthCategoryContainer.NrCiorbeFaraCarne + percentage;
                    continue;
                }

                if (Filters.IsPrajitura(item.Description))
                {
                    monthCategoryContainer.NrPrajituri = monthCategoryContainer.NrPrajituri + percentage;
                    continue;
                }

                if (Filters.IsFruct(item.Description))
                {
                    monthCategoryContainer.NrFructe = monthCategoryContainer.NrFructe + percentage;
                    continue;
                }

                if (Filters.IsPartyMeal(item.Description))
                    continue;

                Util.LogInFile("d://Fel1NotFound", item.Description);
                if (Filters.IsNoChoice(item.Description))
                {
                }
                if (!Filters.IsNoChoice(item.Description))
                    monthCategoryContainer.NrPrajituri = monthCategoryContainer.NrPrajituri + percentage;
            }
        }

        private void ComputeType2(List<MenuItem> menuItems, MonthCategoryContainer monthCategoryContainer,
            double percentage)
        {
            bool postHit = false,
                soiaHit = false,
                salataHit = false,
                puiHit = false,
                porcHit = false,
                vitaHit = false,
                pesteHit = false,
                macaronarHit = false;

            foreach (var item in menuItems)
            {
                if (Filters.IsPostMeal(item.Description))
                {
                    if (!postHit)
                        monthCategoryContainer.NrPost = monthCategoryContainer.NrPost + percentage;
                    postHit = true;
                }

                if (Filters.IsSoiaMeal(item.Description))
                {
                    if (!soiaHit)
                        monthCategoryContainer.NrSoia = monthCategoryContainer.NrSoia + percentage;
                    soiaHit = true;
                    continue;
                }

                if (Filters.IsSalataStandAlone(item.Description))
                {
                    if (!salataHit)
                        monthCategoryContainer.NrSalate = monthCategoryContainer.NrSalate + percentage;
                    salataHit = true;
                    continue;
                }

                if (Filters.IsPuiMeal(item.Description))
                {
                    if (!puiHit)
                        monthCategoryContainer.NrPui = monthCategoryContainer.NrPui + percentage;

                    ComputeDictionaryFromRecipes(GlobalVariables.PuiRecipes, item.Description);
                    puiHit = true;
                    continue;
                }

                if (Filters.IsVitaMeal(item.Description))
                {
                    if (!vitaHit)
                        monthCategoryContainer.NrVita = monthCategoryContainer.NrVita + percentage;
                    vitaHit = true;
                    ComputeDictionaryFromRecipes(GlobalVariables.VitaRecipes, item.Description);
                    continue;
                }

                if (Filters.IsPorcMeal(item.Description))
                {
                    if (!porcHit)
                        monthCategoryContainer.NrPorci = monthCategoryContainer.NrPorci + percentage;

                    ComputeDictionaryFromRecipes(GlobalVariables.PorkRecipes, item.Description);
                    porcHit = true;
                    continue;
                }

                if (Filters.IsPesteMeal(item.Description))
                {
                    if (!pesteHit)
                        monthCategoryContainer.NrPesti = monthCategoryContainer.NrPesti + percentage;
                    pesteHit = true;
                    continue;
                }

                if (Filters.IsPasteMeal(item.Description))
                {
                    if (!macaronarHit)
                        monthCategoryContainer.NrMacaroane = monthCategoryContainer.NrMacaroane + percentage;
                    macaronarHit = true;
                    continue;
                }

                Util.LogInFile("d://Fel2NotFound", item.Description);
                //to add pene lover
                if (!postHit)
                    monthCategoryContainer.NrPost = monthCategoryContainer.NrPost + percentage;
            }
        }
    }
}